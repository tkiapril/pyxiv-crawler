pyxiv-crawler
-------------

pyxiv-crawler is a pixiv crawler based on Python 3.

Usage
-----

Copy config.json.example to config.json and edit as necessary. You need a valid pixiv account to operate this program.
