#!/usr/bin/python
# -*- coding: utf-8 -*-


#
# pyxiv-crawler: a pixiv crawler based on Python 3
# by Tki April
# Added on: 2014-07-08
#


import aiohttp
import asyncio
import json
import os
import sys


USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, ' \
             'like Gecko) Chrome/35.0.1916.153 Safari/537.36'
MAIN_PAGE = 'http://www.pixiv.net/'
LOGIN_PAGE = 'http://www.pixiv.net/login.php'
LIST_PAGE = 'http://www.pixiv.net/member_illust.php?id={target_id}'


@asyncio.coroutine
def login_to_pixiv(id, password):
    cookies = []
    response = yield from aiohttp.request('GET', MAIN_PAGE)
    for cookie in response.headers.getall('SET-COOKIE'):
        cookie = cookie[:cookie.index(';')]
        print('Set-Cookie: {new_cookie}'.format(new_cookie = cookie))
        cookies.append(cookie)
    response = yield from aiohttp.request('POST', LOGIN_PAGE,
                                          headers={'User-Agent': USER_AGENT,
                                                   'DNT': '1',
                                                   'Cookie': '; '.join(cookies),
                                                   'Referer': 'http://pixiv.net/'},
                                          data={'mode': 'login',
                                                'return_to': '/',
                                                'pixiv_id': id,
                                                'pass': password,
                                                'skip': 1})
    print(", ".join(response.headers.keys()))
    for cookie in response.headers.getall('SET-COOKIE'):
        print('Set-Cookie: {new_cookie}'.format(new_cookie = cookie))

@asyncio.coroutine
def crawl_each(sessid, target):
    response = yield from aiohttp.request('GET', LIST_PAGE.format(target_id=target),
                                          headers={'User-Agent': USER_AGENT,
                                                   'DNT': '1',
                                                   'COOKIE': sessid})
    data = yield from response.read_and_close()
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "test-" + target + ".html"), 'wb') as file:
        file.write(data)


def main():
    if len(sys.argv) < 2:
        print('pyxiv-crawler: a pixiv crawler based on Python 3.\n'
              'Please provide pixiv user id to crawl in integer as '
              'a parameter. You can also provide multiple parameters. '
              'Provided ids will be processed in parallel.')
    else:
        targets = sys.argv[1:]
        loop = asyncio.get_event_loop()
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               "config.json")) as config_file:
            config = json.load(config_file)
        sessid = loop.run_until_complete(
            login_to_pixiv(config['credentials']['id'],
                           config['credentials']['password']))
        crawl_coroutines = []
        for i in targets:
            crawl_coroutines.append(crawl_each(sessid, i))
        loop.run_until_complete(asyncio.wait(crawl_coroutines))
        raise(BaseException('This is a stub.'))


if __name__ == '__main__':
    main()
