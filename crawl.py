#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
pyxiv-crawler: a pixiv crawler based on Python 3
by Tki April
Added on: 2014-07-08
"""


from __future__ import print_function


import json
import logging
import os
import re
import sys


from concurrent.futures import ThreadPoolExecutor
from functools import partial


import requests


from bs4 import BeautifulSoup


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
SAVE_DIR = os.path.join(SCRIPT_DIR, 'crawled')


class PyxivCrawler:
    USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, ' \
                 'like Gecko) Chrome/35.0.1916.153 Safari/537.36'
    MAIN_PAGE = 'http://www.pixiv.net/'
    LOGIN_PAGE = 'http://www.pixiv.net/login.php'
    LIST_PAGE = 'http://www.pixiv.net/member_illust.php?' \
                'id={target_id}&p={page_num}'
    BIG_ILLUST_PAGE = 'http://www.pixiv.net/member_illust.php?' \
                      'mode=big&illust_id={illust_id}'
    MEDIUM_ILLUST_PAGE = 'http://www.pixiv.net/member_illust.php?mode=medium' \
                         '&illust_id={illust_id}'

    WORK_URL_REGEX = re.compile(r'/member_illust\.php\?mode=medium&'
                                r'illust_id=([0-9]+)')
    MANGA_URL_REGEX = re.compile(r'member_illust\.php\?mode=manga&'
                                 r'illust_id=([0-9]+)')
    IMAGE_URL_REGEX = re.compile(r'http://i[0-9]+\.pixiv\.net/'
                                 r'img[0-9]+/img/(.+)/[0-9]+\.(.+)')

    def __init__(self, id, password, user_agent=USER_AGENT):
        self.logger = logging.getLogger('PyxivCrawler')
        self.logger.setLevel(logging.INFO)
        self.pixiv = requests.Session()
        self.pixiv.headers.update({'User-Agent': user_agent, 'DNT': '1'})
        self.login_to_pixiv(id, password)

    def login_to_pixiv(self, id, password):
        self.pixiv.get(self.MAIN_PAGE)
        self.pixiv.post(self.LOGIN_PAGE,
                        data={'mode': 'login', 'return_to': '/',
                              'pixiv_id': id, 'pass': password, 'skip': 1},
                        headers={'Referer': self.MAIN_PAGE})
        self.logger.info('Logged in to pixiv. Your cookies are:\n')
        for key, value in self.pixiv.cookies.items():
            self.logger.info('{}: {}\n'.format(key, value))

    def get_user_works(self, target):
        all_works = []
        page_num = 1
        while True:
            response = self.pixiv.get(self.LIST_PAGE.format(target_id=target,
                                                            page_num=page_num))
            self.logger.info('Retrieved list page {} of user {}.'
                             .format(page_num, target))
            soup = BeautifulSoup(response.text)
            works = soup.select('div.display_works > ul '
                                '> li.image-item > a.work')
            if not works:
                break
            page_num += 1
            for work in works:
                all_works.append(self.WORK_URL_REGEX.match(work['href'])
                                 .group(1))
        self.logger.info('There is no work on page {}.'.format(page_num))
        return all_works

    def download_work(self, work, target, target_dir):
        work_type = self.get_work_type(work)
        if work_type == 'ugoira':
            self.logger.warning('Work {} is an Ugoira; '
                                'Ugoira crawling is yet to be implemented.'
                                .format(work))
        elif work_type == 'manga':
            self.logger.warning('Work {} is a Manga; '
                                'Manga crawling is yet to be implemented.'
                                .format(work))
        else:
            illust_url = self.get_original_illust_url(work)
            url_match = self.IMAGE_URL_REGEX.match(illust_url)
            author_screen_name = url_match.group(1)
            extension = url_match.group(2)
            save_file_name = os.path.join(target_dir, '{}_{}_{}.{}'
                                          .format(target, author_screen_name,
                                                  work, extension))
            self.save_illust_to_file(work, save_file_name)
            self.logger.info("Saved to {}.".format(save_file_name))

    def get_work_type(self, work):
        response = self.pixiv.get(self.MEDIUM_ILLUST_PAGE
                                  .format(illust_id=work))
        soup = BeautifulSoup(response.text)
        if soup.select('div._ugoku-illust-player-container'):
            return 'ugoira'
        elif self.MANGA_URL_REGEX.match(soup.select('div.works_display > a')[0]
                                        ['href']):
            return 'manga'
        else:
            return 'illust'

    def get_original_illust(self, work):
        response = self.pixiv.get(self.get_original_illust_url(work),
                                  headers={'Referer': self.BIG_ILLUST_PAGE
                                           .format(illust_id=work)})
        return response.content

    def get_original_illust_url(self, work):
        response = self.pixiv.get(self.BIG_ILLUST_PAGE.format(illust_id=work),
                                  headers={'Referer': self.MEDIUM_ILLUST_PAGE
                                           .format(illust_id=work)})
        soup = BeautifulSoup(response.text)
        return soup.img['src']

    def save_illust_to_file(self, work, filename):
        with open(filename, 'wb') as file:
            file.write(self.get_original_illust(self.pixiv, work))


def main():
    if len(sys.argv) < 2:
        print('pyxiv-crawler: a pixiv crawler based on Python 3.\n'
              'Please provide pixiv user id to crawl in integer as '
              'a parameter. You can also provide multiple parameters. '
              'Provided ids will be processed in parallel.', file=sys.stderr)
        raise SystemExit(1)
    else:
        targets = sys.argv[1:]
        with open(os.path.join(SCRIPT_DIR, 'config.json')) as config_file:
            config = json.load(config_file)
        if not os.path.isdir(SAVE_DIR):
            os.mkdir(SAVE_DIR)
        pixiv = PyxivCrawler(config['credentials']['id'],
                             config['credentials']['password'])
        for target in targets:
            works = pixiv.get_user_works(target)
            works.reverse()
            target_dir = os.path.join(SAVE_DIR, target)
            if not os.path.isdir(target_dir):
                os.mkdir(target_dir)
            download_works_concurrent = partial(
                pixiv.download_work,
                target=target,
                target_dir=target_dir)
            with ThreadPoolExecutor(max_workers=5) as executor:
                executor.map(download_works_concurrent, works)
        print('All crawling completed.')


if __name__ == '__main__':
    main()
